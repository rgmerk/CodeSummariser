import os
from collections import defaultdict
import random


def convert_format(inDir):
    vocab = {}
    f_id = 1
    freq = defaultdict(int)
    train_files = [x.strip() for x in open("splits/train")]
    test_files = [x.strip() for x in open("splits/test")]
    train_size = len(train_files) - 1
    random_train_files = random.sample(range(0, len(train_files)), train_size)
    chosen_train_files = []
    for i, filename in enumerate(train_files):
       if not i in random_train_files:
           continue
       chosen_train_files.append(filename)
       if not filename.endswith(".verbose"):
           continue
       for line in open(os.path.join(inDir, filename)):
           c,f = line.strip().split("\t")
           for ff in f.split():
               if not ff in vocab:
                   vocab[ff] = f_id
                   f_id += 1
               freq[ff] += 1
    vocab["_OOV_"] = f_id
    f_id += 1

    oov_feat = 0 
    for filename in train_files + test_files:
        if not filename.endswith(".verbose"):
            continue
        if filename in train_files:
             if not filename in chosen_train_files: 
                 continue     
             ofilename = filename.replace(".verbose", ".train.liblinear")
        else:
             ofilename = filename.replace(".verbose", ".test.liblinear")
        optr = open(os.path.join(inDir, ofilename), "w")
        for line in open(os.path.join(inDir, filename)):
            c,f = line.strip().split("\t")
            feats = []
            calls = False
            callsmulti = False
            for ff in f.split():
                if ff in vocab:
                    if freq[ff] < 2:
                        feats.append(vocab["_OOV_"])
                    else:
                        feats.append(vocab[ff])
                else:
                      feats.append(vocab["_OOV_"])
                if freq[ff] < 2:
                    oov_feat += 1
            print_line = '-1'
            if c == '1':
                print_line = '+1'
            for ff in sorted(list(set(feats))):
                print_line += ' %d:1' % ff
            optr.write(print_line +'\n')
        optr.close()
    print('oov feat: ', oov_feat) 
    print('total feat: ', f_id)
