'''
Script to extract features and create
liblinear format files 
creates two files 
   1) feat.verbose
       describes all the features and classes
   2) feat.liblinear
       liblinear format
'''
import os
import plyj.parser
import plyj.model as m
from re import finditer
from summarizer import check_dir, invoke_command
from call_graph import *

def _f(kk, vv, checkCamelCase=False):
    if not vv:
        return "NONEFEATURE"
    if checkCamelCase:
        feats = []
        for cc in camel_case_split(vv):
            feats.append("%s:%s" %(kk, cc))
        return " ".join(feats)
    else:
        return "%s:%s" %(kk, vv)

def extract_features(inDir, outDir, parser):
    '''
    inDir has projects/(input, output) pairs
    We are going to do the following 
      for every project
        for every file in input and output
            1) use the ply parser to parse the file
            2) extract classname, method features
            3) use the output to select the class values
    '''
    features_to_extract = [
       "class", #class name
       "class_modifiers", #class modifiers public, private
       "class_implements", #interfaces
       "class_extends", #extends some class
       "method_name_ngram", #ngram word feature
       "method_type_param", #type param
       "method_param", #inputs
       "method_return_type", #return type
       "method_num_variables", #number of variables
       "method_num_function", #number of func calls
       "method_num_lines", #size of method
       "method_incoming_functions", #incoming function size and names
       "method_incoming_function_names", #incoming function size and names
       "method_outgoing_functions", #outgoing function size and names
       "method_outgoing_function_names", #outgoing function size and names
       "method_body_line_types" #statement types
    ]
    training_size = 0
    pexamples = 0
    #iterate over all projects
    for proj in os.listdir(inDir):
        print(proj)
        method_feature_list = []
        rel_path = os.path.join(inDir, proj)
        print (rel_path)
        for input_file in os.listdir( \
                  os.path.join(rel_path, \
                    "input")):
            input_file_abs = os.path.join(rel_path, \
                     "input", input_file)
            print("abs path is: ")
            print (input_file_abs)

            #parse the java code
            cgraph = Callgraph.from_file(input_file_abs, parser)
            method_feature_list += \
                 from_tree(cgraph, features_to_extract)

        training_size += len(method_feature_list)

        method_names_in_summary = []
        for output_file in os.listdir(os.path.join( \
                        rel_path, "output")):
            output_file_abs = os.path.join(rel_path, 
                    "output", output_file)
            tree = parser.parse_file(output_file_abs)
            #update input_method_list with class label
            #class label is whether its part of summary : 1
            #or not : 0
            method_names_in_summary += add_class_labels(tree, 
                                    method_feature_list)

        class_labels = {}
        optr = open(os.path.join(outDir, "%s.verbose" % proj), "w")
        for i, mm in enumerate(method_feature_list):
            if mm[0] in method_names_in_summary:
                 pexamples += 1
                 optr.write("1\t%s\n" %(" ".join(mm[1])))
            else: 
                 optr.write("0\t%s\n" %(" ".join(mm[1])))

        optr.close()

def add_class_labels(tree, 
       method_feature_list):

    method_names_in_summary = []
    if tree is not None:
      for type_decl in tree.type_declarations:
        if not hasattr(type_decl, 'body'):
            continue
        methods = [decl for decl in type_decl.body \
                     if type(decl) is m.MethodDeclaration]
        for method_decl in methods:
            try:
                method_string = str(method_decl)
                if "MethodInvocation" in method_string:
                   method_names_in_summary += \
                     [x.split("'")[0] for x in \
                       method_string.split("MethodInvocation(name='")[1:]]
            except:
               continue
    return method_names_in_summary

def from_tree(cgraph, fte):
    '''
    takes a parse_tree and list of features to extract
    '''
    method_feature_list = []
    tree = cgraph.tree
    if not tree:
        return method_feature_list
    for type_decl in tree.type_declarations:
        class_feature_list = []
        if not hasattr(type_decl, 'name'):
            return method_feature_list
        class_name = type_decl.name
        class_feature_list += handle_class(fte,
                          type_decl)
        class_feature_list += handle_class_modifier(fte,
                          type_decl)
        class_feature_list += handle_class_implements(fte,
                          type_decl)
        class_feature_list += handle_class_extends(fte,
                          type_decl)
        for method_decl in cgraph.nodes:
            feature_list = []
            method_name = method_decl.name
            feature_list += handle_method_ngram(fte,
                               method_decl)
            feature_list += handle_method_return(fte,
                              method_decl)
            feature_list += handle_method_param(fte,
                              method_decl)
            feature_list += handle_method_stats(fte,
                              cgraph, method_decl)
            method_feature_list.append(
              (method_name, 
                  class_feature_list + feature_list))
    return method_feature_list 
    
def handle_method_ngram(fte, method_decl):
    feature_list = []
    method_name = method_decl.name
    if "method_name_ngram" in fte:
        feature_list.append(
          _f("CLASSMETHODNGRAM",
               method_name, checkCamelCase=True))
    return feature_list

def handle_method_return(fte, method_decl):
   feature_list = []
   if "method_return_type" in fte:
       if method_decl.return_val is not None:
            feature_list.append(
                   _f("CLASSMETHODRETURN",
                       method_decl.return_val, 
                         checkCamelCase=True))
   return feature_list

def handle_method_param(fte, method_decl):
    feature_list = []
    if "method_type_param" in fte:
        pkeys = method_decl.params.keys()
        pvalues = method_decl.params.values()
        feature_list.append(_f("CLASSMETHODPARAMCOUNT", 
            str(len(pkeys))))
        for kk, vv in zip(pkeys, pvalues):
            param_name = kk
            param_val = type2str(vv)
            if param_name is not None:
                feature_list.append(
                   _f("CLASSMETHODPARAMNAME",
                     param_name, checkCamelCase=True))
            if param_val is not None:
                feature_list.append(
                   _f("CLASSMETHODPARAMTYPE",
                     param_val, checkCamelCase=True))

    return feature_list     

def handle_method_stats(fte, cgraph, method_decl):
    feature_list = []
    var_count = len(method_decl.params.keys())
    lines_count = len(method_decl.body)
    
    if "method_num_variables" in fte:
        feature_list.append(
          _f("CLASSMETHODVARCOUNT",
             str(var_count)))
    if "method_num_lines" in fte:
        feature_list.append(
          _f("CLASSMETHODLINECOUNT",
             str(lines_count)))
    if "method_body_line_types" in fte:
        for st_type in method_decl.body:
            feature_list.append(
              _f("CLASSMETHODLINETYPE",
                 type2str(st_type)))
    if "method_incoming_functions" in fte:
        in_count = len(cgraph.graph['in'][
                       method_decl.name])
        feature_list.append(
          _f("CLASSMETHODINCOMING",
             str(in_count)))
    if "method_incoming_function_names" in fte:
        for in_name in cgraph.graph['in'][
                       method_decl.name]:
            feature_list.append(
              _f("CLASSMETHODINCOMINGNAME",
                 in_name, checkCamelCase=True))
    if "method_outgoing_functions" in fte:
        out_count = len(cgraph.graph['out'][
                       method_decl.name])
        feature_list.append(
          _f("CLASSMETHODOUTGOING",
             str(out_count)))
    if "method_outgoing_function_names" in fte:
        for out_name in cgraph.graph['out'][
                       method_decl.name]:
            feature_list.append(
              _f("CLASSMETHODOUTGOINGNAME",
                 out_name, checkCamelCase=True))
        
    return feature_list

 
def handle_class(fte, type_decl):
        feature_list = []
        class_name = type_decl.name
        if "class" in fte:
            feature_list.append(_f("CLASSNAME", 
                         class_name))
        return feature_list

def handle_class_modifier(fte, type_decl):
        feature_list = []
        if "class_modifiers" in fte:
            for mm in type_decl.modifiers:
                if type(mm) is str:
                    feature_list.append(_f("CLASSMODIFIER",
                            mm))
        return feature_list

def handle_class_implements(fte, type_decl):
        feature_list = []
        if "class_implements" in fte:
            try:
                if len(type_decl.implements) is 0:
                    feature_list.append(_f("CLASSIMPLEMENTS",
                                 "False"))
                else:
                    feature_list.append(_f("CLASSIMPLEMENTS",
                                 "True"))
                    for tt in type_decl.implements:
                        feature_list.append(_f("CLASSIMPLEMENTNAME",
                              tt.name.value, checkCamelCase=True))
            except: #its an interface
                do_nothing = 1  
        return feature_list

def handle_class_extends(fte, type_decl):
        feature_list = []
        if "class_extends" in fte:
            if not hasattr(type_decl, 'extends'):
                return feature_list
 
            if type_decl.extends is not None:
                feature_list.append(_f("CLASSEXTENDS",
                           "True"))
                if type(type_decl.extends) is list:
                    if len(type_decl.extends) > 0:
                        for tt in type_decl.extends:
                            feature_list.append(
                              _f("CLASSEXTENDNAME",
                                   tt.name.value,
                                     checkCamelCase=True))
                else:
                    feature_list.append(_f("CLASSEXTENDNAME",
                                   type_decl.extends.name.value,
                                     checkCamelCase=True))
                                  
            else:
                feature_list.append(_f("CLASSEXTENDS",
                              "False"))
        return feature_list
        
def camel_case_split(identifier):
    matches = finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', identifier)
    return [m.group(0) for m in matches]

def type2str(s):
    if type(s) == str:
        return s
    else:
       return str(type(s)).split("'")[1]
     
