#!/usr/bin/python
'''
This program is mainly for identifying which projects 
can be used as input, output pair. 
B
Idea is to find projects with "main" and "test" folders
where main folder consists of inputs and test folder consists
of outputs
We take test folder as output since it summarizes which methods are 
important
Usage: python summarizer.py \
          --input <input_dir_with_javacode> 
           --output <output_dir_with_input_output_pairs_identified>
           --tasks all create format extractfeat
'''


import argparse
from argparse import RawTextHelpFormatter
from collections import defaultdict
import logging
import plyj.parser
import os
import sys
import subprocess
from convert_format import *
from extract_features_callgraph import *
import random

def check_dir(adir):
    #creates directories
    if not os.path.isdir(adir):
        invoke_command(['mkdir', adir])

def check_patterns(options):
    '''
    Function to compare patterns with
    directory sturcture
    '''
    for subdir in os.listdir(options.input):
        output_files = defaultdict(list)
        for root, _ , files in \
              os.walk(
               os.path.join(options.input, subdir)):
            #check if input and output exist
            for pattern_type in options.patterns.keys():
                for pp in options.patterns[pattern_type]:
                    if "/%s/" % pp in root:
                        for fname in files:
                            #yield pattern_type, subdir, root, fname
                            output_files[pattern_type] \
                                 .append(
                                    (pattern_type,
                                      subdir, root,
                                        fname))
        if len(output_files["input"]) > 0 and \
             len(output_files["output"]) > 0:
              for kk in output_files.keys():
                 for vv in output_files[kk]:
                    yield vv
           

def process_dirs(options):
    '''
    Function to create input output paris
    Takes input and patterns to identify 
    input output pairs and creates output 
    folder
    We take the children dir of input dir as 
    the package name
    '''
    #check_input_pattern returns type=input/output
    # and package name and filename to copy
    for pattern_type, pname, root, fname in \
          check_patterns(options):
        package_outdir = os.path.join(options.output,
                              pname)
        check_dir(package_outdir)
        check_dir(os.path.join(package_outdir, pattern_type))
        invoke_command(['cp', 
            os.path.join(root, fname), 
              os.path.join(package_outdir, pattern_type)])
                
def invoke_command(cmd_array):
   logging.debug('Run command: %s' 
        %(' '.join(cmd_array)))
   subprocess.check_call(cmd_array)
 
if __name__ == '__main__':
   #Create a argparser
   parser = argparse.ArgumentParser(
             argument_default=False, 
              description="Script to \
                identify input, output pairs",
               formatter_class=RawTextHelpFormatter)
   parser.add_argument('--input', '-i', 
                action='store', 
                 required=True,
                  metavar='input',  
                    help='input java files dir')
   parser.add_argument('--output', '-o',
                 action='store', 
                  required=True,
                  metavar='output',
                   help='output folder with input \
                     output pair')
   parser.add_argument('--tasks', '-t', 
                   choices=['all','store', 'extractfeat', 'test', 'format'],
                   nargs='+',
                   help='tasks to run')
   parser.add_argument('--log', '-l', 
                  action='store',
                   metavar='loglevel',
                    default='info',
                     choices=['debug', 'info', 
                               'warning'],
                      help='debug level')
   options = parser.parse_args()
   random.seed(100)
   #Set logging level
   numeric_level = getattr(logging,
                     options.log.upper())
   if not isinstance(numeric_level, int):
       ValueError('Invalid log level %s' 
                      % options.log)
   logging.basicConfig(level=numeric_level)

    
   #verify input and output
   if not os.path.isdir(options.input):
      IOError('Invalid input folder %s' 
           % options.input)
   logging.info('Input: %s' % options.input)
   check_dir(options.output)
   check_dir(os.path.join(options.output, "data"))
   feat_output = os.path.join(options.output, "features")
   options.output = os.path.join(options.output, "data")

   logging.info('Output: %s' % options.output)

   #Configure input and output subdirectory patterns
   #these patterns would be used to find input, 
   # and output code pairs
   options.patterns = {
       'input' : ['main'],
       'output' : ['test']
   }
   #process inputs and outputs
   runAll = False
   if len(options.tasks) == 0 \
       or 'all' in options.tasks:
       runAll = True

   if runAll \
       or "store" in options.tasks:
       #find the prospective input, output pairs
       process_dirs(options) 

   #extract features from the input files
   if runAll \
      or "extractfeat" in options.tasks:
       check_dir(feat_output)
       parser = plyj.parser.Parser()
       extract_features(options.output, 
             feat_output, parser)
   #format the features into liblinear format
   if runAll or \
        "format" in options.tasks:
       convert_format(feat_output)
