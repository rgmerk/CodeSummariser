package javaclassdescription;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import simplenlg.aggregation.ClauseCoordinationRule;
import simplenlg.features.Feature;
import simplenlg.features.NumberAgreement;
import simplenlg.framework.CoordinatedPhraseElement;
import simplenlg.framework.DocumentElement;
import simplenlg.framework.NLGElement;
import simplenlg.framework.NLGFactory;
import simplenlg.lexicon.Lexicon;
import simplenlg.phrasespec.NPPhraseSpec;
import simplenlg.phrasespec.PPPhraseSpec;
import simplenlg.phrasespec.SPhraseSpec;
import simplenlg.phrasespec.VPPhraseSpec;
import simplenlg.realiser.english.Realiser;

public class TestMain {
	public static void main(String[] args) throws IOException, ParseException, org.json.simple.parser.ParseException {
	  FileReader in = new FileReader(args[0]);
	  JSONParser parser = new JSONParser();
	  JSONObject jsonObject = (JSONObject) parser.parse(in);

	  for (Object key : jsonObject.keySet()) {
	        String keyStr = (String)key;
	        JSONObject keyvalue = (JSONObject) jsonObject.get(keyStr);
	        generateClassDescription(keyStr, keyvalue);
	       
	    }
	}
	
	/* generates the general description of a class */
	public static String generateClassSummary(NLGFactory nlgFactory, Realiser realiser, String name, JSONObject fields) {
		/* Class description: main clause */
	      SPhraseSpec classDescription = nlgFactory.createClause();
	      NPPhraseSpec className = nlgFactory.createNounPhrase(name);
		  VPPhraseSpec verbBe = nlgFactory. createVerbPhrase("be");
	      NPPhraseSpec classType = nlgFactory.createNounPhrase("class");
	      classType.addModifier(fields.get("modifier"));
	      classType.setDeterminer("a");
	      classDescription.setSubject(className);
	      classDescription.setVerb(verbBe);
	      classDescription.setObject(classType);
	      
	      /* Class description: relative clause (implements and extends */
	      CoordinatedPhraseElement implementsAndExtends = nlgFactory.createCoordinatedPhrase();
  	      if (!fields.get("implements").equals("False")  || !fields.get("extends").equals("False")) {
	    	  if (!fields.get("implements").equals("False")) {
	    		  SPhraseSpec classImplements = nlgFactory.createClause();
	    		  VPPhraseSpec verbImplements = nlgFactory. createVerbPhrase("implement");
	    	      NPPhraseSpec objectImplements = nlgFactory.createNounPhrase(fields.get("implements"));
	    	      classImplements.setVerb(verbImplements);
	    	      classImplements.setObject(objectImplements);
	    	      implementsAndExtends.addCoordinate(classImplements);
	    	  }
	    	  if (!fields.get("extends").equals("False")) {
	    		  SPhraseSpec classExtends = nlgFactory.createClause();
	    		  VPPhraseSpec verbExtends = nlgFactory. createVerbPhrase("extend");
	    	      NPPhraseSpec objectExtends = nlgFactory.createNounPhrase(fields.get("extends"));
	    	      classExtends.setVerb(verbExtends);
	    	      classExtends.setObject(objectExtends);
	    	      implementsAndExtends.addCoordinate(classExtends);
	    	  }		      
	      }
	      classDescription.addComplement(implementsAndExtends);
	      String output = realiser.realiseSentence(classDescription); 
	      return output;
	}
	
	/* generates a short summary of all the methods of a class */
	public static String generateMethodsSummary(NLGFactory nlgFactory, Realiser realiser, String name, JSONObject fields) {
		/* Method summary */
	      SPhraseSpec methodSummary = nlgFactory.createClause();
	      VPPhraseSpec verbBe = nlgFactory. createVerbPhrase("be");
	      NPPhraseSpec methodCount = nlgFactory.createNounPhrase();
	      CoordinatedPhraseElement methodList = nlgFactory.createCoordinatedPhrase();
	      
	      /* Subject: the ... methods of ... */
	      Integer nMethods = ((JSONArray)fields.get("methods")).size();
	      
	      methodCount.setDeterminer("the");
	      methodCount.setNoun("method");
	      if (nMethods > 1) {
	    	  methodCount.setFeature(Feature.NUMBER, NumberAgreement.PLURAL);
	    	  methodCount.addPreModifier(nMethods.toString());
		  }
	      else {
	    	  methodCount.addPreModifier("only");
	      }
	      
	      PPPhraseSpec genitiveClass = nlgFactory.createPrepositionPhrase("of");
	      genitiveClass.addComplement(name);
	      methodCount.addComplement(genitiveClass);
	      
	      for (Object method: ((JSONArray)fields.get("methods"))) {
	    	  NPPhraseSpec methodName = nlgFactory.createNounPhrase();
	    	  
	    	  JSONObject methodObject = (JSONObject)method;
	    	  methodName.setNoun(methodObject.get("name").toString());
	    	  methodName.addPostModifier("("+methodObject.get("return")+")");
	    	  
	    	  methodList.addCoordinate(methodName);
	      }
	      
	      methodSummary.setSubject(methodCount);
	      methodSummary.setVerb(verbBe);
	      methodSummary.setObject(methodList);
	      String output = realiser.realiseSentence(methodSummary); 
	      return output;
	}
	
	
	/* generates a list of description of each method */
	public static NLGElement generateMethodDescription(NLGFactory nlgFactory, Realiser realiser, JSONObject methodObject){
		 NPPhraseSpec methodName = nlgFactory.createNounPhrase();
	   	  methodName.setNoun(methodObject.get("name").toString());
		  SPhraseSpec methodArguments = nlgFactory.createClause();
	   	  VPPhraseSpec verbTake = nlgFactory. createVerbPhrase("take");
	      methodArguments.setSubject(methodName);
	      methodArguments.setVerb(verbTake);
	   	  
	   	  /* direct object of "X takes ... arguments */
	      CoordinatedPhraseElement argumentList = nlgFactory.createCoordinatedPhrase();
	      Integer nArguments = ((JSONArray)methodObject.get("params")).size();
	      NPPhraseSpec argumentCount = nlgFactory.createNounPhrase();
	   	  argumentCount.setNoun("argument");
	      
	      if (nArguments == 0) {
	    	  argumentCount.setFeature(Feature.NUMBER, NumberAgreement.PLURAL);
	    	  argumentCount.addPreModifier("no");
	      }
	      else if (nArguments > 1) {
	    	  argumentCount.setFeature(Feature.NUMBER, NumberAgreement.PLURAL);
	    	  argumentCount.addPreModifier(nArguments.toString());
	    	  argumentList.addPreModifier("(");
		      argumentList.addPostModifier(")");
		      
		  }
	      else {
	    	  argumentCount.addPreModifier("only one");
	    	  argumentList.addPreModifier("(");
		      argumentList.addPostModifier(")");
	      }
	      
	      /* actual list of arguments */
	      for (Object argument: ((JSONArray)methodObject.get("params"))) {
	    	  NPPhraseSpec argumentName = nlgFactory.createNounPhrase();
	    	  JSONObject argumentObject = (JSONObject)argument;
	    	  argumentName.setNoun(argumentObject.get("name").toString());
	    	  argumentName.addPreModifier(argumentObject.get("type").toString());
	    	  argumentList.addCoordinate(argumentName);
	      }
	      argumentCount.addPostModifier(argumentList);
	      methodArguments.setObject(argumentCount);

	      /* coordinate clause: ... and returns ... */
	      SPhraseSpec methodReturns = nlgFactory.createClause();
	      VPPhraseSpec verbReturn = nlgFactory. createVerbPhrase("return");
	      methodReturns.setSubject(methodName);
	      methodReturns.setObject(methodObject.get("return"));
	   	  methodReturns.setVerb(verbReturn);
	   	  
	   	  ClauseCoordinationRule methodDescription = new ClauseCoordinationRule();
	   	  methodDescription.setFactory(nlgFactory);
	   	  return methodDescription.apply(methodArguments, methodReturns);
	}

	/* generated a description of how the method is used in terms of its incoming and outgoing calls */
	public static NLGElement generateMethodUsage(NLGFactory nlgFactory, Realiser realiser, JSONObject methodObject){
		  NPPhraseSpec methodName = nlgFactory.createNounPhrase();
	   	  methodName.setNoun(methodObject.get("name").toString());
		  SPhraseSpec methodOutgoing = nlgFactory.createClause();
	   	  VPPhraseSpec verbCall = nlgFactory. createVerbPhrase("call");
	      methodOutgoing.setSubject(methodName);
	      methodOutgoing.setVerb(verbCall);
	   	  
	   	  /* Outgoing methods */
	      CoordinatedPhraseElement outgoingList = nlgFactory.createCoordinatedPhrase();
	      Integer nOutgoing = ((JSONArray)methodObject.get("outgoing")).size();
	      NPPhraseSpec outgoingCount = nlgFactory.createNounPhrase();
	      outgoingCount.setNoun("method");

	      if (nOutgoing == 0) {
	    	  outgoingCount.setFeature(Feature.NUMBER, NumberAgreement.PLURAL);
	    	  outgoingCount.addPreModifier("no");
	      }
	      else if (nOutgoing > 1) {
	    	  outgoingCount.setFeature(Feature.NUMBER, NumberAgreement.PLURAL);
	    	  outgoingCount.addPreModifier(nOutgoing.toString());
	    	  outgoingList.addPreModifier(":");

		  }
	      else {
	    	  outgoingCount.addPreModifier("only one");
	    	  outgoingList.addPreModifier(":");
	      }
	      
	      /* actual list of methods */
	      for (Object outgoing: ((JSONArray)methodObject.get("outgoing"))) {
	    	  NPPhraseSpec outgoingName = nlgFactory.createNounPhrase();
	    	  JSONObject outgoingObject = (JSONObject)outgoing;
	    	  outgoingName.setNoun(outgoingObject.get("name").toString());
	    	  outgoingList.addCoordinate(outgoingName);
	      }
	      
	      outgoingCount.addPostModifier(outgoingList);
	      methodOutgoing.setObject(outgoingCount);

	      /* coordinate clause: Incoming methods */
		  SPhraseSpec methodIncoming = nlgFactory.createClause();
		  methodIncoming.setObject(methodName);
		  methodIncoming.setVerb(verbCall);
	   	   
	   	  CoordinatedPhraseElement incomingList = nlgFactory.createCoordinatedPhrase();
	      Integer nIncoming = ((JSONArray)methodObject.get("incoming")).size();
	      NPPhraseSpec incomingCount = nlgFactory.createNounPhrase();
	      incomingCount.setNoun("method");

	      if (nIncoming == 0) {
	    	  incomingCount.setFeature(Feature.NUMBER, NumberAgreement.PLURAL);
	    	  incomingCount.addPreModifier("no");
	      }
	      else if (nIncoming > 1) {
	    	  incomingCount.setFeature(Feature.NUMBER, NumberAgreement.PLURAL);
	    	  incomingCount.addPreModifier(nIncoming.toString());
	    	  incomingList.addPreModifier("(");
	    	  incomingList.addPostModifier(")");
		  }
	      else {
	    	  incomingCount.addPreModifier("only one");
	    	  incomingList.addPreModifier("(");
	    	  incomingList.addPostModifier(")");
	      }
	      
	      /* actual list of methods */
	      for (Object incoming: ((JSONArray)methodObject.get("incoming"))) {
	    	  NPPhraseSpec incomingName = nlgFactory.createNounPhrase();
	    	  JSONObject incomingObject = (JSONObject)incoming;
	    	  incomingName.setNoun(incomingObject.get("name").toString());
	    	  incomingList.addCoordinate(incomingName);
	      }
	      incomingCount.addPostModifier(incomingList);
	      methodIncoming.setSubject(incomingCount);
	      
	   	  CoordinatedPhraseElement methodUsage = nlgFactory.createCoordinatedPhrase();
		  methodUsage.addCoordinate(methodIncoming);
		  methodUsage.addCoordinate(methodOutgoing);
		  
	   	  return methodUsage;
	   	  
	}
	
	/* created a list of method descriptions */
	public static String generateMethodDescriptions(NLGFactory nlgFactory, Realiser realiser, JSONObject fields){
		List<DocumentElement> methodDescriptions = new ArrayList<DocumentElement>();
		
		for (Object method: ((JSONArray)fields.get("methods"))) {
			  NLGElement methodDescription = generateMethodUsage(nlgFactory, realiser, (JSONObject)method);
			  //NLGElement methodDescription = generateMethodDescription(nlgFactory, realiser, (JSONObject)method);
	    	  DocumentElement methodDescriptionSentence = nlgFactory.createSentence(methodDescription);
	    	  methodDescriptions.add(methodDescriptionSentence);
	      }
		DocumentElement methodDescriptionParagraph = nlgFactory.createParagraph(methodDescriptions);
		String output = realiser.realise(methodDescriptionParagraph).getRealisation();
		return output;
	}
	
	/* main generation function: generate in three steps and compose the final output */
	public static void generateClassDescription(String name, JSONObject fields) {
		  Lexicon lexicon = Lexicon.getDefaultLexicon();
		  NLGFactory nlgFactory = new NLGFactory(lexicon);
		  Realiser realiser = new Realiser(lexicon);
	      
		  String output;
		  output = generateClassSummary(nlgFactory, realiser, name, fields);
		  System.out.println(output);
		  output = generateMethodsSummary(nlgFactory, realiser, name, fields);
		  System.out.println(output);
		  output = generateMethodDescriptions(nlgFactory, realiser, fields);
		  System.out.println(output);
	}

}
