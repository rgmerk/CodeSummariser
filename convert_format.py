import os
from collections import defaultdict
import random


def convert_format(inDir, data):
    vocab = {}
    f_id = 1
    freq = defaultdict(int)

    for c, f in data:
        for ff in f:#.split():
            if not ff in vocab:
                vocab[ff] = f_id
                f_id += 1
            freq[ff] += 1
    vocab["_OOV_"] = f_id
    f_id += 1

    oov_feat = 0
    #print "chosen_train_files", chosen_train_files
    
    vectors = []
    labels = []
    optr = open(os.path.join(inDir, "output.txt"), "w")
    for c, f in data:
        feats = []

        #print 'Starting loop'
        for ff in f:
            if ff in vocab:
                if freq[ff] < 2:
                    feats.append(vocab["_OOV_"])
                else:
                    feats.append(vocab[ff])
            else:
                  feats.append(vocab["_OOV_"])
            if freq[ff] < 2:
                oov_feat += 1
        #print 'Ending loop'
        print_line = '-1'
        if c == '1':
            print_line = '+1'
        svm_line = print_line
        for ff in sorted(list(set(feats))):
            print_line += ' %d:1' % ff 
            svm_line += str(ff)

        vectors.append(list(set(feats)))
        labels.append(int(print_line[:2]))
        optr.write(print_line +'\n')
    optr.close()
    print('oov feat: ', oov_feat) 
    print('total feat: ', f_id)
    # Randomized labels until we won't find way to label items
    #labels = [random.randint(0, 1) for i in range(len(labels))]
    for i in range(100):
        labels[i] = 0
    return labels, vectors
