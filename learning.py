# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 01:25:57 2018

@author: NAJAM
"""

import numpy as np
import pandas as pd
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.cross_validation import KFold
from sklearn.metrics import precision_score, recall_score, f1_score
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.feature_selection.rfe import RFECV
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2, mutual_info_classif, f_classif

# count accuracy score for SVC predictor on cross-validation dataset
def hyperopt_train_test(X, y, params):
    kf = KFold(len(X),n_folds=5,random_state=1, shuffle=True)

    all_results = []
    for train_index, test_index in kf:
        X_train = X[train_index,:]
        X_test = X[test_index,:]
        y_train = y[train_index]
        y_test = y[test_index]

        clf = svm.SVC(**params)
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        all_results.append(accuracy_score(y_test, y_pred))
        
    res = np.mean(all_results)
    
    return res

def tune_hyperparams(X, y):
    kernels = ('linear',)# 'poly', 'rbf', 'sigmoid')
    rnd_states = (9875, )
    probabilities = (True,)# False)
    Cs = [1]#, 10, 100, 1000]
    gammas = [0.001]#, 0.0001, 'auto']
    decision_function_shapes = ['ovo']#, 'ovr']
    max_res = -1
    best_param = None
    
    # let's tune hyperparameters to find a proper ones
    for C in Cs:
        for decision_function_shape in decision_function_shapes:
            for gamma in gammas:
                for kernel in kernels:
                    for probability in probabilities:
                        for random_state in rnd_states:
                            params = {'C': C, 'decision_function_shape': decision_function_shape,
                                      'gamma': gamma, 'kernel': kernel,
                                      'probability':probability, 'random_state': random_state}
                            res = hyperopt_train_test(X, y, params)
                            #print 'accuracy', res
                            if res > max_res:
                                max_res = res
                                print "cross validation accuracy:", max_res
                                best_param = params
    return best_param

def run_train(vectors, labels, test_file="./Output\features\test.test.liblinear"):
    def _feature_selectioning(model, X, y):
        selector = RFECV(model)
        selector.fit(X, y)
        return selector.ranking_
   
    max_ff = max([max(item) for item in vectors])
    #print 'max', max_ff
    #print len(vectors), len(labels)
    
    #print "Labels:", len(labels)
    X = np.zeros((len(labels), max_ff + 1))
    y = np.array(labels)
    
    for i, v in enumerate(vectors):
        for x in v:
            X[i][x] += 1
    #X = pd.DataFrame(X)
    best_params = tune_hyperparams(X, y)

    X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.2, random_state=42)
    
    print "Len of train part:", len(y_train)
    
    clf = svm.SVC(**best_params)
    clf.fit(X_train, y_train)
    #ranks = _feature_selectioning(clf, X, y)
    #print 'ranking', ranks
    
    #ranks = [x, i for i, x in enumerate(ranks)]
    print 'Whole feature set'
    y_pred = clf.predict(X_test)
    
    print("Precision {}".format(precision_score(y_test, y_pred)))
    print("Recall {}".format(recall_score(y_test, y_pred)))
    print("F1 {}".format(f1_score(y_test, y_pred)))

    print X_test.shape
    # Performing feature selection for subset of features using different
    # models to eliminate extra-features
    idx = 0
    for method in (chi2, mutual_info_classif, f_classif):
        #print type(X_train)
        if method == chi2:
            for k in (100, 300, 600, 800, 1000, 1100):
                skb = SelectKBest(method, k=k)
                X_new = skb.fit_transform(X_train, y_train)
                X_test_new = skb.transform(X_test)
                idx += 1
                print "Feature set", idx
                clf = svm.SVC(**best_params)
                clf.fit(X_new, y_train)
                y_pred = clf.predict(X_test_new)
                
                print("Accuracy score:", accuracy_score(y_test, y_pred))
                print("Precision {}".format(precision_score(y_test, y_pred)))
                print("Recall {}".format(recall_score(y_test, y_pred)))
                print("F1 {}".format(f1_score(y_test, y_pred)))
        else:
            skb = SelectKBest(method)
            X_new = skb.fit_transform(X_train, y_train)
            X_test_new = skb.transform(X_test)
            idx += 1
            print "Feature set", idx
            clf = svm.SVC(**best_params)
            clf.fit(X_new, y_train)
            y_pred = clf.predict(X_test_new)
            
            print("Accuracy score:", accuracy_score(y_test, y_pred))
            print("Precision {}".format(precision_score(y_test, y_pred)))
            print("Recall {}".format(recall_score(y_test, y_pred)))
            print("F1 {}".format(f1_score(y_test, y_pred)))
