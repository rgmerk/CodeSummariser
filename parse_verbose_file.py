#!/usr/bin/env python

# writes a JSON structure on the standard output
# usage: parse_verbose_file.py VERBOSE FILE

import sys
import simplejson as json

javaClasses = dict()

def parse_line(line):
    # read the file
    index, fields = line.split('\t')
    features = [{'name':field.split(':')[0],
               'value':field.split(':')[1]} for field in fields.split(' ')]

    # build the method dictionary
    method = dict()

    # default values
    method['name'] = ""
    currentparamname = ""
    currentparamtype = ""
    method['params'] = []
    class_implements = ""
    class_modifier = ""
    class_extends = ""
    class_implements_name = ""
    class_extends_name = ""
    method['implements_name'] = ""
    method['incoming'] = []
    method['outgoing'] = []
    for feature in features:
        if feature['name'] == 'CLASSNAME':
            class_name = feature['value']
        if feature['name'] == 'CLASSMODIFIER':
            class_modifier = feature['value']
        if feature['name'] == 'CLASSIMPLEMENTS':
            class_implements = feature['value']
        if feature['name'] == 'CLASSIMPLEMENTNAME':
            class_implements_name += feature['value']
        if feature['name'] == 'CLASSEXTENDS':
            class_extends = feature['value']
        if feature['name'] == 'CLASSEXTENDNAME':
            class_extends_name = feature['value']
        if feature['name'] == 'CLASSMETHODNGRAM':
            method['name'] += feature['value']
        if feature['name'] == 'CLASSMETHODRETURN':
            method['return'] = feature['value']
        if feature['name'] == 'CLASSMETHODPARAMNAME':
            if currentparamname != "" and currentparamtype != "":
                method['params'].append({'name':currentparamname, 'type':currentparamtype})
                currentparamname = ""
                currentparamtype = ""
            currentparamname += feature['value']
        if feature['name'] == 'CLASSMETHODPARAMTYPE':
            currentparamtype += feature['value']
        if feature['name'] == 'CLASSMETHODINCOMINGNAME':
            method['incoming'].append({'name':feature['value']})
        if feature['name'] == 'CLASSMETHODOUTGOINGNAME':
            method['outgoing'].append({'name':feature['value']})

    if class_implements_name != "":
        class_implements = class_implements_name
    if class_extends_name != "":
        class_extends = class_extends_name

    # update the class dictionary
    if not class_name in javaClasses:
        javaClasses[class_name] = {'name': class_name,
                                             'modifier': class_modifier,
                                             'implements': class_implements,
                                             'extends': class_extends,
                                             'methods' : []}
    javaClasses[class_name]['methods'].append(method)

# read from file (first argument)
with open(sys.argv[1]) as f:
    for line in f:
        parse_line(line.strip())

# write on the standard output
print json.dumps(javaClasses)
